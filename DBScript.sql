DROP DATABASE IF EXISTS ExamDB;
CREATE DATABASE ExamDB;
USE ExamDB;

CREATE TABLE Admin(
	AdminId BIGINT(6) UNSIGNED,
	First_name VARCHAR(25) NOT NULL,
	Last_name VARCHAR(50) NOT NULL,
    Username VARCHAR(50) NOT NULL,
    Pass VARCHAR(50) NOT NULL,
    Email VARCHAR(255) NOT NULL,
	PRIMARY KEY(Email)
) engine = InnoDB;

CREATE TABLE Teacher(
	TeacherId BIGINT(6) UNSIGNED,
    SocialSecurityNumber VARCHAR(12) NOT NULL,
    First_name VARCHAR(25) NOT NULL,
    Last_name VARCHAR(50) NOT NULL,
    Email VARCHAR(50) NOT NULL,
    Phone_number VARCHAR(20) NOT NULL,
    PRIMARY KEY(SocialSecurityNumber)
) engine = InnoDB;


CREATE TABLE Section(
	SectionId BIGINT(2) AUTO_INCREMENT,
    Section_name VARCHAR(25) NOT NULL,
    Pass VARCHAR(25) NOT NULL,
    PRIMARY KEY(SectionId)
) engine = InnoDB;

CREATE TABLE Adult(
	AdultId BIGINT(6) UNSIGNED,
    SocialSecurityNumber VARCHAR(12) NOT NULL,
    First_name VARCHAR(25) NOT NULL,
    Last_name VARCHAR(50) NOT NULL,
    Email VARCHAR(50) NOT NULL,
    Phone_number VARCHAR(20) NOT NULL,
    Work_number VARCHAR(20) NOT NULL,
    Role VARCHAR(25) NOT NULL,
    PRIMARY KEY(SocialSecurityNumber)
) engine = InnoDB;

CREATE TABLE Child(
	ChildId BIGINT(6) UNSIGNED,
    SectionId BIGINT(2) NOT NULL,
    SocialSecurityNumber VARCHAR(12) NOT NULL,
    First_name VARCHAR(25) NOT NULL,
    Last_name VARCHAR(50) NOT NULL,
    PRIMARY KEY(SocialSecurityNumber),
    FOREIGN KEY(SectionId) REFERENCES Section(SectionId)
) engine = InnoDB;

CREATE TABLE Attendance(
	AttendanceId BIGINT(3) UNSIGNED AUTO_INCREMENT,
    ChildSocialSecurityNumber VARCHAR(12) NOT NULL,
    TeacherSocialSecurityNumber VARCHAR(12) NOT NULL,
    PickedUpBySSN VARCHAR(12),
    AttendanceDate VARCHAR(10) NOT NULL,
    Present BOOLEAN NOT NULL,
    PRIMARY KEY(AttendanceId),
    FOREIGN KEY(ChildSocialSecurityNumber) REFERENCES Child(SocialSecurityNumber),
    FOREIGN KEY(TeacherSocialSecurityNumber) REFERENCES Teacher(SocialSecurityNumber),
    FOREIGN KEY(PickedUpBySSN) REFERENCES Adult(SocialSecurityNumber)
) engine = InnoDB;

CREATE TABLE Present (
	P_ID BIGINT AUTO_INCREMENT,
    C_SSN VARCHAR(12) NOT NULL,
    P_Date DATE NOT NULL,
    Arrival VARCHAR(6) NOT NULL,
    ClockOut VARCHAR(6),
    PRIMARY KEY(P_ID),
    FOREIGN KEY(C_SSN) REFERENCES Child(SocialSecurityNumber)
) engine = InnoDB;

CREATE TABLE Absent (
	A_ID BIGINT NOT NULL,
    C_SSN VARCHAR(12) NOT NULL,
    A_Date DATE NOT NULL,
    PRIMARY KEY(C_SSN)
) engine = InnoDB;

CREATE TABLE Sick (
	S_ID BIGINT NOT NULL,
    C_SSN VARCHAR(12) NOT NULL,
    S_Date DATE NOT NULL,
    PRIMARY KEY(C_SSN)
) engine = InnoDB;

CREATE TABLE Emergency(
	EmergencyId INT(3) UNSIGNED AUTO_INCREMENT,
    EmergencyDate DATETIME NOT NULL,
    SectionId BIGINT(2) NOT NULL,
    PRIMARY KEY(EmergencyId),
    FOREIGN KEY(SectionId) REFERENCES Section(SectionId)
) engine = InnoDB;

CREATE TABLE Child_Message (
	Id BIGINT AUTO_INCREMENT,
    CSSN VARCHAR(12) NOT NULL,
    Message VARCHAR(255),
    PRIMARY KEY(Id),
    FOREIGN KEY(CSSN) REFERENCES Child(SocialSecurityNumber)
) engine = InnoDB;

CREATE TABLE Child_Adult (
	CSSN VARCHAR(12) NOT NULL,
    ASSN VARCHAR(12) NOT NULL,
    FOREIGN KEY(CSSN) REFERENCES Child(SocialSecurityNumber),
    FOREIGN KEY(ASSN) REFERENCES Adult(SocialSecurityNumber)
) engine = InnoDB;

INSERT INTO Section(Section_name, Pass) VALUES("Krabban", "pass");
INSERT INTO Section(Section_name, Pass) VALUES("Fjärillen", "pass");

INSERT INTO Teacher(SocialSecurityNumber, First_name, Last_name, Email, Phone_number) VALUES(19940617, "Kevin", "Nemec", "Testmail@gmail.com", 0766335532);
INSERT INTO Teacher(SocialSecurityNumber, First_name, Last_name, Email, Phone_number) VALUES(941201, "RR", "RR", "namharr@hotmail.com", 0707251548);

/*Adults*/
INSERT INTO Adult(SocialSecurityNumber, First_name, Last_name, Email, Role, Phone_number, Work_number) VALUES ("197003029432", "Haraki", "Kandas", "Haraki@hotmail.com", "Father", "07663323532", "07663355013");
INSERT INTO Adult(SocialSecurityNumber, First_name, Last_name, Email, Role, Phone_number, Work_number) VALUES ("197023521734", "Savana", "Kandas", "Savana@hotmail.com", "Mother", "0766331232", "076144124");
INSERT INTO Adult(SocialSecurityNumber, First_name, Last_name, Email, Role, Phone_number, Work_number) VALUES ("195008020303", "Abdullah", "Kandas", "Abdullah@hotmail.com", "Family", "0756565525", "0736565255");

INSERT INTO Adult(SocialSecurityNumber, First_name, Last_name, Email, Role, Phone_number, Work_number) VALUES ("19850505", "Joell", "Ragnarök", "Zara@hotmail.com", "Father", "0345535345", "07012364587");
INSERT INTO Adult(SocialSecurityNumber, First_name, Last_name, Email, Role, Phone_number, Work_number) VALUES ("19800321", "Stina", "Ragnarök", "Pelle@hotmail.com", "Mother", "02344324", "045655456");
INSERT INTO Adult(SocialSecurityNumber, First_name, Last_name, Email, Role, Phone_number, Work_number) VALUES ("19941201", "Khristian", "Ragnarök", "Khristian@hotmail.com", "Friend", "754754664", "23452555");


/*Section Krabban Child*/
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (1, 1, "201406010739", "Mohammed", "Kandas");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (2, 1, "201407170312", "Mohammed", "Khalid");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (3, 1, "201408170314", "Ahmed", "Kondos");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (4, 1, "201409170202", "Namir", "Fugejzy");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (5, 1, "201410170621", "Nour", "Salim");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (6, 1, "201411170412", "Farad", "Kalim");

INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (7, 1, "201504177665", "Aishah", "Aretha");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (8, 1, "201503174345", "Abia", "Aretha");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (9, 1, "201502172464", "Asha", "Aretha");

/*Section Fjärillen Child*/
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (10, 2, "201411011111", "Julia", "Svensson");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (11, 2, "201404033888", "Alice", "Andersson");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (12, 2, "201501017777", "Maja", "Sten");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (13, 2, "201503025545", "Ella", "Axelsson");

INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (14, 2, "201428019978", "Oscar", "Nilsson");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (15, 2, "201506057876", "Lucas", "Lundgren");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (16, 2, "201505058678", "Oliver", "Stigsson");
INSERT INTO Child(ChildId, SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (17, 2, "201504208436", "Noah", "Ragnarök");

/*Admins*/
INSERT INTO Admin(First_name, Last_name, Username, Pass, Email) VALUES ("Admin", "Adminovic", "1", "1", "Admin@admin.se");
INSERT INTO Admin(First_name, Last_name, Username, Pass, Email) VALUES ("3xR", "Programman", "3xR", "1", "rrahman.rexhepi@hotmail.com");
INSERT INTO Admin(First_name, Last_name, Username, Pass, Email) VALUES ("Nesho", "DenNemec", "nesho", "1", "kevin.nemec94@gmail.com");

/*Child And Adult Relation*/
INSERT INTO Child_Adult VALUES ("201406010739", "197003029432"); /*Family Kandas*/ 
INSERT INTO Child_Adult VALUES ("201406010739", "197023521734"); /*Family Kandas*/ 	
INSERT INTO Child_Adult VALUES ("201406010739", "195008020303"); /*Family Kandas*/ 

INSERT INTO Child_Adult VALUES ("201504208436", "19850505"); /*Family RagnarRök*/
INSERT INTO Child_Adult VALUES ("201504208436", "19800321"); /*Family RagnarRök*/
INSERT INTO Child_Adult VALUES ("201504208436", "19941201"); /*Family RagnarRök*/

SELECT A.First_name AS AFN, A.Last_name AS ALN, A.SocialSecurityNumber AS ASSN, A.Phone_number AS APN, A.Work_number AS AWN, A.Email AS AEM, A.Role AS ROLE 
FROM Child_Adult CA
INNER JOIN Adult A ON CA.ASSN = A.SocialSecurityNumber 
WHERE CA.CSSN = "20140617";


/* GET ALL ABSENT/DUE TO ARRIVE CHILDREN IN A SECTION FOR A SPECIFIC DATE - Child POJO */
SELECT C.ChildId AS CID, C.SectionId AS CSID, C.First_name AS CFN, C.Last_name AS CLN, C.SocialSecurityNumber AS CSSN 
FROM Child C 
INNER JOIN Section S ON S.SectionId = C.SectionId 
WHERE  NOT EXISTS
  (SELECT P.C_SSN, S.Section_name 
    FROM Present P
    WHERE P.C_SSN = C.SocialSecurityNumber AND P_Date = '2017-05-10' AND S.Section_name = "Krabban")
AND S.Section_name = "Krabban";
    

/* GET ALL PRESENT CHILDREN IN A SECTION FOR A SPECIFIC DATE - Child POJO */
SELECT C.ChildId AS CID, C.SectionId AS CSID, C.First_name AS CFN, C.Last_name AS CLN, C.SocialSecurityNumber AS CSSN, 
P.Arrival AS Arrival, P.ClockOut AS ClockOut 
FROM Child C 
INNER JOIN Present P ON P.C_SSN = C.SocialSecurityNumber 
INNER JOIN Section S ON S.SectionId = C.SectionId 
WHERE EXISTS
(SELECT P.C_SSN, S.Section_name
	FROM Present P
    WHERE P.C_SSN = C.SocialSecurityNumber AND P_Date = '2017-05-10' AND S.Section_name = "Fjärillen")
AND S.Section_name = "Fjärillen";
    
SELECT * FROM Adult ORDER BY First_name;