package newton.se.TibetanMastiff.model;

public class Attendance {
	private long attendanceId;
	private Adult adult;
	private Child child;
	private Teacher teacher;
	private String attendanceDate;
	
	public Attendance(long attendanceId, Child child, Teacher teacher, Adult adult, String attendanceDate){
		this.attendanceId = attendanceId;
		this.child = child;
		this.teacher = teacher;
		this.adult = adult;
		this.attendanceDate = attendanceDate;
	}
	public Attendance(){}
	
	public long getAttendanceId() {
		return attendanceId;
	}
	public void setAttendanceId(long attendanceId) {
		this.attendanceId = attendanceId;
	}
	public Child getChild() {
		return child;
	}
	public void setChild(Child child) {
		this.child = child;
	}
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	public Adult getAdult() {
		return adult;
	}
	public void setAdult(Adult adult) {
		this.adult = adult;
	}
	public String getAttendanceDate() {
		return attendanceDate;
	}
	public void setAttendanceDate(String attendanceDate) {
		this.attendanceDate = attendanceDate;
	}
}
