package newton.se.TibetanMastiff.model;

public class UserToken {
	
	String username;
	String password;
	
	public UserToken (String username, String password) {
		this.password  = password;
		this.username = username;
	}
	
	public UserToken() {
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}

