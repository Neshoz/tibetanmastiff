package newton.se.TibetanMastiff.model;

public class Sick {
	private long sickId;
	private Section section;
	private Child child;
	private String date;
	
	public Sick(long sickId, Section section, Child child, String date) {
		this.sickId = sickId;
		this.section = section;
		this.child = child;
		this.date = date;
	}
	public Sick() {
		
	}
	
	public long getSickId() {
		return sickId;
	}
	public void setSickId(long sickId) {
		this.sickId = sickId;
	}
	public Section getSection() {
		return section;
	}
	public void setSection(Section section) {
		this.section = section;
	}
	public Child getChild() {
		return child;
	}
	public void setChild(Child child) {
		this.child = child;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
}
