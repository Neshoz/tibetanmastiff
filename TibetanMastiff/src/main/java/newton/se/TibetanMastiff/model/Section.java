package newton.se.TibetanMastiff.model;

public class Section {
	private long sectionId;
	private String sectionName;
	private String password;
	
	public Section(long sectionId, String sectionName, String password){
		this.sectionId = sectionId;
		this.sectionName = sectionName;
		this.password = password;
	}
	public Section(long sectionId, String sectionName) {
		this.sectionId = sectionId;
		this.sectionName = sectionName;
	}
	public Section(String sectionName) {
        this.sectionName = sectionName;
    }
	public Section(){}

	public long getSectionId() {
		return sectionId;
	}
	public void setSectionId(long sectionId) {
		this.sectionId = sectionId;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
