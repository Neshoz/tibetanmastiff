package newton.se.TibetanMastiff.model;

import java.util.Date;

public class Emergency {
	private int emergencyId;
	private Date emergencyDate;
	private Section section;
	
	public Emergency(int emergencyId, Section section){
		this.emergencyId = emergencyId;
		this.section = section;
		this.emergencyDate = new Date();
	}
	public Emergency(){}

	public int getEmergencyId() {
		return emergencyId;
	}
	public void setEmergencyId(int emergencyId) {
		this.emergencyId = emergencyId;
	}
	public Date getEmergencyDate() {
		return emergencyDate;
	}
	public void setEmergencyDate(Date emergencyDate) {
		this.emergencyDate = emergencyDate;
	}
	public Section getSection(){
		return section;
	}
	public void setSection(Section section){
		this.section = section;
	}
}
