package newton.se.TibetanMastiff.model;

public class ClockOut {
    private String time;

    public ClockOut(String time) {
        this.time = time;
    }
    public ClockOut() {
    	
    }

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
    	this.time = time;
    }
}