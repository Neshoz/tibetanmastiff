package newton.se.TibetanMastiff.model;

import java.util.ArrayList;
import java.util.List;

public class Child {
	private long childId;
	private long sectionId;
	private String socialSecurityNumber;
	private List<Adult> parents;
	private String firstName;
	private String lastName;
	private String arrival;
	private String clockOut;
	
	public Child(long childId, long sectionId, String socialSecurityNumber, String firstName, String lastName, String arrival, String clockOut) {
		this.childId = childId;
		this.sectionId = sectionId;
		this.socialSecurityNumber = socialSecurityNumber;
		this.parents = new ArrayList<Adult>();
		this.firstName = firstName;
		this.lastName = lastName;
		this.arrival = arrival;
		this.clockOut = clockOut;
	}
	public Child(long childId, long sectionId, String socialSecurityNumber, String firstName, String lastName, String arrival) {
		this.childId = childId;
		this.sectionId = sectionId;
		this.socialSecurityNumber = socialSecurityNumber;
		this.parents = new ArrayList<Adult>();
		this.firstName = firstName;
		this.lastName = lastName;
		this.arrival = arrival;
	}
	public Child(long childId, long sectionId, String socialSecurityNumber, String firstName, String lastName) {
		this.childId = childId;
		this.sectionId = sectionId;
		this.socialSecurityNumber = socialSecurityNumber;
		this.parents = new ArrayList<Adult>();
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public Child(long sectionId, String socialSecurityNumber, String firstName, String lastName) {
		this.sectionId = sectionId;
		this.socialSecurityNumber = socialSecurityNumber;
		this.parents = new ArrayList<Adult>();
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public Child(String socialSecurityNumber, String firstName, String lastName) {
		this.socialSecurityNumber = socialSecurityNumber;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public Child(){}

	public long getChildId() {
		return childId;
	}
	public void setChildId(long childId) {
		this.childId = childId;
	}
	public long getSectionId() {
		return sectionId;
	}
	public void setSectionId(long sectionId) {
		this.sectionId = sectionId;
	}
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}
	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public List<Adult> getParents() {
		return parents;
	}
	public void setParents(List<Adult> parents) {
		this.parents = parents;
	}
	public String getArrival() {
		return arrival;
	}
	public void setArrival(String arrival) {
		this.arrival = arrival;
	}
	public String getClockOut() {
		return clockOut;
	}
	public void setClockOut(String clockOut) {
		this.clockOut = clockOut;
	}
	public boolean isPresent() {
		return this.clockOut != null;
	}
}
