package newton.se.TibetanMastiff.service.adult;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import newton.se.TibetanMastiff.model.Adult;

/*
 * DataAcceslayer for Adult.
 * */
public class AdultServiceImpl implements AdultService {

	@Override
	public List<Adult> getAllAdults(Connection conn) throws SQLException {
		List<Adult> adults = new LinkedList<Adult>();
		String query = "SELECT * FROM Adult ORDER BY First_name";

		Statement statement = conn.createStatement();
		ResultSet result = statement.executeQuery(query);

		while (result.next()) {
			adults.add(new Adult(result.getString("First_name"),
					result.getString("Last_name"), result.getString("SocialSecurityNumber"), result.getString("Email"), result.getString("Phone_number"),
					result.getString("Work_number"), result.getString("Role")));
		}
		if (!statement.getConnection().isClosed()) {
			statement.close();
		}

		return adults;
	}

	@Override
	public Adult getAdult(Connection conn, String socialSecurityNumber) throws SQLException {
		String query = "SELECT * FROM Adult WHERE SocialSecurityNumber = ?";
		Adult adult = null;

		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, socialSecurityNumber);
		ResultSet result = statement.executeQuery();

		if (result.next()) {
			adult = new Adult(result.getString("First_name"),
					result.getString("Last_name"), result.getString("SocialSecurityNumber"),  result.getString("Email"), result.getString("Phone_number"),
					result.getString("Work_number"), result.getString("Role"));
		}
		if (!statement.getConnection().isClosed()) {
			statement.close();
		}
		return adult;
	}

	@Override
	public void postAdult(Connection conn, String childSSN, Adult adult) throws SQLException {
		int executedUpdate = 0;
		String query = "INSERT INTO Adult(SocialSecurityNumber, First_name, Last_name, Email, Phone_Number, Work_number, Role) VALUES (?,?,?,?,?,?,?)";

		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, adult.getSocialSecurityNumber());
		statement.setString(2, adult.getFirstName());
		statement.setString(3, adult.getLastName());
		statement.setString(4, adult.getEmail());
		statement.setString(5, adult.getPhoneNumber());
		statement.setString(6, adult.getWorkPhoneNumber());
		statement.setString(7, adult.getRole());
		executedUpdate = statement.executeUpdate();
		
		if(executedUpdate > 0) {
			String sql = "INSERT INTO Child_Adult VALUES (?, ?)";
			statement = conn.prepareStatement(sql);
			statement.setString(1, childSSN);
			statement.setString(2, adult.getSocialSecurityNumber());
			executedUpdate = statement.executeUpdate();
			
			if(executedUpdate > 0 && statement.getConnection().isClosed()) {
				statement.close();
			}
		}
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
	}

	@Override
	public void deleteAdult(Connection conn, Adult adult) throws SQLException {
		int executedUpdate = 0;
		
		String sql = "DELETE FROM Child_Adult WHERE ASSN = ?";
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1, adult.getSocialSecurityNumber());
		executedUpdate = statement.executeUpdate();
		
		if(executedUpdate > 0) {
			String query = "DELETE FROM Adult WHERE SocialSecurityNumber = ?";
			statement = conn.prepareStatement(query);

			statement.setString(1, adult.getSocialSecurityNumber());
			statement.executeUpdate();

			if (!statement.getConnection().isClosed()) {
				statement.close();
			}
		}
	}

	@Override
	public void updateAdultInformation(Connection conn, String socialSecurityNumber, Adult adult) throws SQLException {
		String query = "UPDATE Adult SET First_name=?, Last_name=?, Email=?, Phone_number=?, Work_number=?, Role=? WHERE SocialSecurityNumber =?";

		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, adult.getFirstName());
		statement.setString(2, adult.getLastName());
		statement.setString(3, adult.getEmail());
		statement.setString(4, adult.getPhoneNumber());
		statement.setString(5, adult.getWorkPhoneNumber());
		statement.setString(6, adult.getRole());
		statement.setString(7, adult.getSocialSecurityNumber());

		statement.executeUpdate();

		if (!statement.getConnection().isClosed()) {
			statement.close();
		}
	}

}
