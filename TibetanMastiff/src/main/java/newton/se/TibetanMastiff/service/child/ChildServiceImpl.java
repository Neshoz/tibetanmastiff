package newton.se.TibetanMastiff.service.child;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import newton.se.TibetanMastiff.model.Adult;
import newton.se.TibetanMastiff.model.Child;

public class ChildServiceImpl implements ChildService {

	@Override
	public List<Child> getAllChildren(Connection conn) throws SQLException {
		List<Child> children = new LinkedList<Child>();
		String query = "SELECT C.SectionId, C.SocialSecurityNumber, C.First_name, C.Last_name FROM Child C ";

		Statement statement = conn.createStatement();
		ResultSet result = statement.executeQuery(query);

		while (result.next()) {
			Child child = new Child(result.getLong("SectionId"), result.getString("SocialSecurityNumber"),
					result.getString("First_name"), result.getString("Last_name"));

			children.add(child);
		}

		if (!statement.getConnection().isClosed()) {
			statement.close();
		}
		if(!result.isClosed()) {
			result.close();
		}

		return children;
	}

	@Override
	public List<Child> getAllChildrenInSection(Connection conn, String sectionName) throws SQLException {
		List<Child> children = new LinkedList<Child>();
		String query = "SELECT SectionId FROM Section WHERE Section_name = ?";

		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, sectionName);
		ResultSet result = statement.executeQuery();

		if (result.next()) {
			long sectionId = result.getLong("SectionId");

			query = "SELECT C.SectionId, C.SocialSecurityNumber, C.First_name, C.Last_name "
					+ "FROM Child C "
					+ "WHERE C.SectionId = ?";
			statement = conn.prepareStatement(query);
			statement.setLong(1, sectionId);
			result = statement.executeQuery();

			while (result.next()) {
				Child child = new Child(result.getLong("SectionId"), result.getString("SocialSecurityNumber"),
						result.getString("First_name"), result.getString("Last_name"));

				children.add(child);
			}
		}

		if (!statement.getConnection().isClosed()) {
			statement.close();
		}
		if(!result.isClosed()) {
			result.close();
		}

		return children;
	}

	@Override
	public Child getChild(Connection conn, String socialSecurityNumber) throws SQLException {
		Child child = null;
		String sql = "SELECT C.SectionId, C.SocialSecurityNumber, C.First_name, C.Last_name FROM Child C WHERE C.SocialSecurityNumber = ?";

		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1, socialSecurityNumber);
		ResultSet result = statement.executeQuery();

		if (result.next()) {
			child = new Child(result.getLong("SectionId"), result.getString("SocialSecurityNumber"),
					result.getString("First_name"), result.getString("Last_name"));

		}

		if (!statement.getConnection().isClosed()) {
			statement.close();
		}

		return child;
	}

	@Override
	public void postChild(Connection conn, String sectionName, Child child) throws SQLException {
		int executedUpdate = 0;
		String query = "SELECT SectionId FROM Section WHERE Section_name = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, sectionName);
		ResultSet result = statement.executeQuery();
		
		if(result.next()) {
			child.setSectionId(result.getLong("SectionId"));
			
			String sql = "INSERT INTO Child(SectionId, SocialSecurityNumber, First_name, Last_name) VALUES (?, ?, ?, ?)";
			statement = conn.prepareStatement(sql);
			statement.setLong(1, child.getSectionId());
			statement.setString(2, child.getSocialSecurityNumber());
			statement.setString(3, child.getFirstName());
			statement.setString(4, child.getLastName());
			executedUpdate = statement.executeUpdate();

			if (executedUpdate > 0 && !statement.getConnection().isClosed()) {
				statement.close();
			}
		}
		if(!result.isClosed()) {
			result.close();
		}
	}

	@Override
	public void deleteChild(Connection conn, Child child) throws SQLException {
		String query = "DELETE FROM Child WHERE SocialSecurityNumber = ?";

		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, child.getSocialSecurityNumber());
		statement.executeUpdate();

		if (!statement.getConnection().isClosed()) {
			statement.close();
		}
	}

	@Override
	public void updateChild(Connection conn, Child child) throws SQLException {
		String query = "UPDATE Child SET SectionId=?,First_name=?, Last_name =? WHERE SocialSecurityNumber=?";

		PreparedStatement statement = conn.prepareStatement(query);
		statement.setLong(1, child.getSectionId());
		statement.setString(2, child.getFirstName());
		statement.setString(3, child.getLastName());
		statement.setString(4, child.getSocialSecurityNumber());

		statement.executeUpdate();

		if (!statement.getConnection().isClosed()) {
			statement.close();
		}

	}


	@Override
	public List<Adult> getAllowedPickUpAdults(Connection conn, String socialSecurityNumber) throws SQLException {
		List<Adult> adults = new ArrayList<Adult>();
		String query = "SELECT A.First_name AS AFN, A.Last_name AS ALN, A.SocialSecurityNumber AS ASSN, A.Phone_number AS APN, A.Work_number AS AWN, A.Email AS AEM, A.Role AS ROLE "
				+ "FROM Child_Adult CA "
				+ "INNER JOIN Adult A ON CA.ASSN = A.SocialSecurityNumber "
				+ "WHERE CA.CSSN = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, socialSecurityNumber);
		ResultSet result = statement.executeQuery();
		
		while(result.next()) {
			String firstName = result.getString("AFN");
			String lastName = result.getString("ALN");
			String ssn = result.getString("ASSN");
			String phoneNumber = result.getString("APN");
			String workNumber = result.getString("AWN");
			String email = result.getString("AEM");
			String role = result.getString("ROLE");
			
			adults.add(new Adult(firstName, lastName, ssn, email, phoneNumber, workNumber, role));
		}
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
		if(!result.isClosed()) {
			result.close();
		}
		
		return adults;
	}

}
