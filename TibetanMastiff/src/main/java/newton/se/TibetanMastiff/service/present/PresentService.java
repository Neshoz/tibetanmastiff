package newton.se.TibetanMastiff.service.present;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import newton.se.TibetanMastiff.model.Child;
import newton.se.TibetanMastiff.model.ClockOut;
import newton.se.TibetanMastiff.model.Present;

public interface PresentService {
	List<Present> getAllPresentChildren(Connection conn, String date) throws SQLException;
	List<Present> getPresentChildrenInSection(Connection conn, String section, String date) throws SQLException;
	
	Present getPresentChild(Connection conn, String socialSecurityNumber, String date, String querySection) throws SQLException;
	
	void postPresentAttendance(Connection conn, Present present) throws SQLException;
	void clockOutPresentChild(Connection conn, String socialSecurityNumber, String date, String clockOutTime) throws SQLException;
}
