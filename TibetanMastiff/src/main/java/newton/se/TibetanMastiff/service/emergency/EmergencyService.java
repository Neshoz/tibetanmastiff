package newton.se.TibetanMastiff.service.emergency;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import newton.se.TibetanMastiff.model.Emergency;

public interface EmergencyService {
	List<Emergency> getAllEmergencies(Connection conn) throws SQLException;
	Emergency getEmergency(Connection conn, int emergencyId) throws SQLException;
	void postEmergency(Connection conn, Emergency emergency) throws SQLException;
}
