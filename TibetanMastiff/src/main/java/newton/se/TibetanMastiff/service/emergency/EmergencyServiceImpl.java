package newton.se.TibetanMastiff.service.emergency;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import newton.se.TibetanMastiff.model.Emergency;
import newton.se.TibetanMastiff.model.Section;

public class EmergencyServiceImpl implements EmergencyService {

	@Override
	public List<Emergency> getAllEmergencies(Connection conn) throws SQLException {
		List<Emergency> emergencyList = new LinkedList<Emergency>();
		String query = "SELECT Emergency.EmergencyId, Section.Section_name, Emergency.SectionId FROM Emergency INNER JOIN Section ON Emergency.SectionId = Section.SectionId";
		
		Statement statement = conn.createStatement();
		ResultSet result = statement.executeQuery(query);
		
		while(result.next()){
			emergencyList.add(new Emergency(result.getInt("EmergencyId"), new Section(result.getInt("SectionId"), result.getString("Section_name"))));
		}
		if(!statement.getConnection().isClosed()){
			statement.close();
		}
		
		return emergencyList;
	}
	
	@Override
	public Emergency getEmergency(Connection conn, int emergencyId) throws SQLException {
		Emergency emergency = null;
		String query = "SELECT Emergency.EmergencyId, Section.SectionId, Section.Section_name FROM Emergency INNER JOIN Section ON Section.SectionId = Emergency.SectionId WHERE Emergency.EmergencyId = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setInt(1, emergencyId);
		ResultSet result = statement.executeQuery();
		
		if(result.next()){
			emergency = new Emergency(result.getInt("EmergencyId"), new Section(result.getInt("SectionId"), result.getString("Section_name")));
		}
		if(!statement.getConnection().isClosed()){
			statement.close();
		}
		
		return emergency;
	}

	@Override
	public void postEmergency(Connection conn, Emergency emergency) throws SQLException {
		String query = "INSERT INTO Emergency(EmergencyId, EmergencyDate, SectionId) VALUES (?, ?, ?)";
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setInt(1, emergency.getEmergencyId());
		statement.setDate(2, (java.sql.Date) emergency.getEmergencyDate());
		statement.setLong(3, emergency.getSection().getSectionId());
		statement.executeUpdate();
		
		if(!statement.getConnection().isClosed()){
			statement.close();
		}
	}


}
