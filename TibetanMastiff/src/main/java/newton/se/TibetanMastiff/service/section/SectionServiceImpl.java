package newton.se.TibetanMastiff.service.section;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import newton.se.TibetanMastiff.model.Section;

public class SectionServiceImpl implements SectionService {

	@Override
	public List<Section> getAllSections(Connection conn) throws SQLException {
		List<Section> sections = new LinkedList<Section>();
		String query = "SELECT * FROM Section";
		
		Statement statement = conn.createStatement();
		ResultSet result = statement.executeQuery(query);
		
		while(result.next()){
			sections.add(new Section(result.getInt("SectionId"), result.getString("Section_name"), result.getString("pass")));
		}
		if(!statement.getConnection().isClosed()){
			statement.close();
		}
		
		return sections;
	}

	@Override
	public Section getSection(Connection conn, String sectionName) throws SQLException {
		String query = "SELECT * FROM Section WHERE Section_name = ?";
		Section section = null;
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, sectionName);
		ResultSet result = statement.executeQuery();
		
		if(result.next()){
			section = new Section(result.getInt("SectionId"), result.getString("Section_name"), result.getString("pass"));
		}
		if(!statement.getConnection().isClosed()){
			statement.close();
		}
		
		return section;
	}

	@Override
	public void postSection(Connection conn, Section section) throws SQLException {
		String query = "INSERT INTO Section(Section_name, Pass) VALUES (?, ?)";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, section.getSectionName());
		statement.setString(2, section.getPassword());
		statement.executeUpdate();
		
		if(!statement.getConnection().isClosed()){
			statement.close();
		}
	}
	
	@Override
	public void updateSection(Connection conn, Section section) throws SQLException {
		int execute = 0;
		String sql = "UPDATE Section SET Section_name = ?, Pass = ? WHERE SectionId = ?";
		
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1, section.getSectionName());
		statement.setString(2, section.getPassword());
		statement.setLong(3, section.getSectionId());
		execute = statement.executeUpdate();
		
		if(execute > 0 && !statement.getConnection().isClosed()) {
			statement.close();
		}
	}
}
