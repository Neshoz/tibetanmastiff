package newton.se.TibetanMastiff.service.absent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import newton.se.TibetanMastiff.model.Absent;
import newton.se.TibetanMastiff.model.Adult;
import newton.se.TibetanMastiff.model.Child;
import newton.se.TibetanMastiff.model.Section;

public class AbsentServiceImpl implements AbsentService {

	@Override
	public List<Absent> getAllAbsentChildren(Connection conn, String date) throws SQLException {
		List<Absent> absentChildren = new ArrayList<Absent>();
		String query = "SELECT A_ID, A_Date, "
				+ "C.SocialSecurityNumber AS CSSN, C.First_name AS CFN, C.Last_name AS CLN, "
				+ "S.SectionId AS SId, S.Section_name AS SName, "
				+ "AD.AdultId AS ADId, AD.SocialSecurityNumber AS ADSSN, AD.First_name AS ADFN, AD.Last_name AS ADLN, AD.Email AS ADEmail, AD.Phone_number AS ADPN, AD.Work_number AS ADWN, AD.Role as ADRole, "
				+ "AM.AdultId AS AMId, AM.SocialSecurityNumber AS AMSSN, AM.First_name AS AMFN, AD.Last_name AS AMLN, AM.Email AS AMEmail, AM.Phone_number AS AMPN, AM.Work_number AS AMWN, AM.Role AS AMRole "
				+ "FROM Absent "
				+ "INNER JOIN Child C ON Absent.C_SSN = C.SocialSecurityNumber "
				+ "INNER JOIN Section S ON C.SectionId = S.SectionId "
				+ "INNER JOIN Adult AD ON C.FatherSocialSecurityNumber = AD.SocialSecurityNumber "
				+ "INNER JOIN Adult AM ON C.MotherSocialSecurityNumber = AM.SocialSecurityNumber "
				+ "WHERE A_Date = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, date);
		ResultSet result = statement.executeQuery();
		
		while(result.next()) {
			long absentId = result.getLong("A_ID");
			String absentDate = result.getString("A_Date");
			
			String childSSN = result.getString("CSSN");
			String childFN = result.getString("CFN");
			String childLN = result.getString("CLN");
			
			long sectionId = result.getLong("SID");
			String sectionName = result.getString("sName");
			
			long adId = result.getLong("ADId");
			String adSSN = result.getString("ADSSN");
			String adPN = result.getString("ADPN");
			String adWN = result.getString("ADWN");
			String adFN = result.getString("ADFN");
			String adLN = result.getString("ADLN");
			String adEmail = result.getString("ADEmail");
			String adRole = result.getString("ADRole");
			Adult father = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
			
			adId = result.getLong("AMId");
			adSSN = result.getString("AMSSN");
			adPN = result.getString("AMPN");
			adWN = result.getString("AMWN");
			adFN = result.getString("AMFN");
			adLN = result.getString("AMLN");
			adEmail = result.getString("AMEmail");
			adRole = result.getString("AMRole");
			Adult mother = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
			
			Section section = new Section(sectionId, sectionName);
			Child child = new Child(sectionId, childSSN, childFN, childLN);
			
			child.getParents().add(father);
			child.getParents().add(mother);
			
			absentChildren.add(new Absent(absentId, section, child, absentDate));
		}
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
		
		return absentChildren;
	}

	@Override
	public List<Absent> getAbsentChildrenInSection(Connection conn, String date, String querySection) throws SQLException {
		List<Absent> absentChildren = new ArrayList<Absent>();
		String query = "SELECT A_ID, A_Date, "
				+ "C.SocialSecurityNumber AS CSSN, C.First_name AS CFN, C.Last_name AS CLN, "
				+ "S.SectionId AS SId, S.Section_name AS SName, "
				+ "AD.AdultId AS ADId, AD.SocialSecurityNumber AS ADSSN, AD.First_name AS ADFN, AD.Last_name AS ADLN, AD.Email AS ADEmail, AD.Phone_number AS ADPN, AD.Work_number AS ADWN, AD.Role as ADRole, "
				+ "AM.AdultId AS AMId, AM.SocialSecurityNumber AS AMSSN, AM.First_name AS AMFN, AD.Last_name AS AMLN, AM.Email AS AMEmail, AM.Phone_number AS AMPN, AM.Work_number AS AMWN, AM.Role AS AMRole "
				+ "FROM Absent "
				+ "INNER JOIN Child C ON Absent.C_SSN = C.SocialSecurityNumber "
				+ "INNER JOIN Section S ON C.SectionId = S.SectionId "
				+ "INNER JOIN Adult AD ON C.FatherSocialSecurityNumber = AD.SocialSecurityNumber "
				+ "INNER JOIN Adult AM ON C.MotherSocialSecurityNumber = AM.SocialSecurityNumber "
				+ "WHERE Absent.A_Date = ? AND S.Section_name = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, date);
		statement.setString(2, querySection);
		ResultSet result = statement.executeQuery();
		
		while(result.next()) {
			long absentId = result.getLong("A_ID");
			String absentDate = result.getString("A_Date");
			
			String childSSN = result.getString("CSSN");
			String childFN = result.getString("CFN");
			String childLN = result.getString("CLN");
			
			long sectionId = result.getLong("SID");
			String sectionName = result.getString("sName");
			
			long adId = result.getLong("ADId");
			String adSSN = result.getString("ADSSN");
			String adPN = result.getString("ADPN");
			String adWN = result.getString("ADWN");
			String adFN = result.getString("ADFN");
			String adLN = result.getString("ADLN");
			String adEmail = result.getString("ADEmail");
			String adRole = result.getString("ADRole");
			Adult father = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
			
			adId = result.getLong("AMId");
			adSSN = result.getString("AMSSN");
			adPN = result.getString("AMPN");
			adWN = result.getString("AMWN");
			adFN = result.getString("AMFN");
			adLN = result.getString("AMLN");
			adEmail = result.getString("AMEmail");
			adRole = result.getString("AMRole");
			Adult mother = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
			
			Section section = new Section(sectionId, sectionName);
			Child child = new Child(sectionId, childSSN, childFN, childLN);
			
			child.getParents().add(father);
			child.getParents().add(mother);
			
			absentChildren.add(new Absent(absentId, section, child, absentDate));
		}
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
		
		return absentChildren;
	}

	@Override
	public Absent getAbsentChild(Connection conn, String socialSecurityNumber, String date, String querySection) throws SQLException {
		Absent absentChild = null;
		String query = "SELECT A_ID, C_SSN, A_Date, "
				+ "C.SocialSecurityNumber AS CSSN, C.First_name AS CFN, C.Last_name AS CLN, "
				+ "S.SectionId AS SId, S.Section_name AS SName, "
				+ "AD.AdultId AS ADId, AD.SocialSecurityNumber AS ADSSN, AD.First_name AS ADFN, AD.Last_name AS ADLN, AD.Email AS ADEmail, AD.Phone_number AS ADPN, AD.Work_number AS ADWN, AD.Role as ADRole, "
				+ "AM.AdultId AS AMId, AM.SocialSecurityNumber AS AMSSN, AM.First_name AS AMFN, AD.Last_name AS AMLN, AM.Email AS AMEmail, AM.Phone_number AS AMPN, AM.Work_number AS AMWN, AM.Role AS AMRole "
				+ "FROM Absent "
				+ "INNER JOIN Child C ON Absent.C_SSN = C.SocialSecurityNumber "
				+ "INNER JOIN Section S ON C.SectionId = S.SectionId "
				+ "INNER JOIN Adult AD ON C.FatherSocialSecurityNumber = AD.SocialSecurityNumber "
				+ "INNER JOIN Adult AM ON C.MotherSocialSecurityNumber = AM.SocialSecurityNumber "
				+ "WHERE Absent.A_Date = ? AND Absent.C_SSN = ? AND S.Section_name = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, date);
		statement.setString(2, socialSecurityNumber);
		statement.setString(3, querySection);
		ResultSet result = statement.executeQuery();
		
		if(result.next()) {
			long absentId = result.getLong("A_ID");
			String absentDate = result.getString("A_Date");
			
			String childSSN = result.getString("CSSN");
			String childFN = result.getString("CFN");
			String childLN = result.getString("CLN");
			
			long sectionId = result.getLong("SID");
			String sectionName = result.getString("sName");
			
			long adId = result.getLong("ADId");
			String adSSN = result.getString("ADSSN");
			String adPN = result.getString("ADPN");
			String adWN = result.getString("ADWN");
			String adFN = result.getString("ADFN");
			String adLN = result.getString("ADLN");
			String adEmail = result.getString("ADEmail");
			String adRole = result.getString("ADRole");
			Adult father = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
			
			adId = result.getLong("AMId");
			adSSN = result.getString("AMSSN");
			adPN = result.getString("AMPN");
			adWN = result.getString("AMWN");
			adFN = result.getString("AMFN");
			adLN = result.getString("AMLN");
			adEmail = result.getString("AMEmail");
			adRole = result.getString("AMRole");
			Adult mother = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
			
			Section section = new Section(sectionId, sectionName);
			Child child = new Child(sectionId, childSSN, childFN, childLN);
			
			child.getParents().add(father);
			child.getParents().add(mother);
			
			absentChild = new Absent(absentId, section, child, absentDate);
		}
		
		return absentChild;
	}

	@Override
	public void postAbsentAttendance(Connection conn, Absent absent) throws SQLException {
		String sql = "INSERT INTO Absent(A_ID, C_SSN, A_Date) VALUES (?, ?, ?)";
		PreparedStatement statement = conn.prepareCall(sql);
		statement.setLong(1, absent.getAbsentId());
		statement.setString(2, absent.getChild().getSocialSecurityNumber());
		statement.setString(3, absent.getDate());
		statement.executeUpdate();
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
	}
}
