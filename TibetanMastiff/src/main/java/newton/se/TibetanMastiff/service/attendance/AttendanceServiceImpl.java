package newton.se.TibetanMastiff.service.attendance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import newton.se.TibetanMastiff.model.Adult;
import newton.se.TibetanMastiff.model.Attendance;
import newton.se.TibetanMastiff.model.AttendanceOverview;
import newton.se.TibetanMastiff.model.Child;
import newton.se.TibetanMastiff.model.ChildMessage;
import newton.se.TibetanMastiff.model.ClockOut;
import newton.se.TibetanMastiff.model.Present;
import newton.se.TibetanMastiff.model.Teacher;

public class AttendanceServiceImpl implements AttendanceService {

	@Override
	public List<Attendance> getAllAttendances(Connection conn) throws SQLException {
		List<Attendance> attendanceList = new ArrayList<Attendance>();
		String query = "SELECT AttendanceId, AttendanceDate, isAttending, Arrival, ExpectedClockOut, ClockOut, "
				+ "C.SectionId AS ChildSection, C.SocialSecurityNumber AS ChildSSN, C.First_name AS ChildFN, C.last_name AS ChildLN, "
				+ "T.First_name AS TeacherFN, T.Last_name AS TeacherLN, T.Email AS TeacherMail, T.Phone_number AS TeacherPN, T.SocialSecurityNumber AS TeacherSSN, "
				+ "A.SocialSecurityNumber AS AdultSSN, A.First_name AS AdultFN, A.Last_name AS AdultLN, A.Email AS AdultEmail, A.Phone_number AS AdultPN, A.Work_number AS AdultWN, A.Role AS AdultRole "
				+ "FROM Attendance "
				+ "INNER JOIN Child C ON Attendance.ChildSocialSecurityNumber = C.SocialSecurityNumber "
				+ "INNER JOIN Teacher T ON Attendance.TeacherSocialSecurityNumber = T.SocialSecurityNumber "
				+ "INNER JOIN Adult A ON Attendance.PickedUpBySSN = A.SocialSecurityNumber";
		
		Statement statement = conn.createStatement();
		ResultSet result = statement.executeQuery(query);
		
		while(result.next()){
			long childSection = result.getLong("ChildSection");
			String childSSN = result.getString("ChildSSN");
			String childFirstName = result.getString("ChildFN");
			String childLastName = result.getString("ChildLN");
			
			String teacherFirstName = result.getString("TeacherFN");
			String teacherLastName = result.getString("TeacherLN");
			String teacherEmail = result.getString("TeacherMail");
			String teacherSSN = result.getString("TeacherSSN");
			String teacherPhoneNumber = result.getString("TeacherPN");
			
			String adultSSN = result.getString("AdultSSN");
			String adultFirstName = result.getString("AdultFN");
			String adultLastName = result.getString("AdultLN");
			String adultMail = result.getString("AdultEmail");
			String adultPhoneNumber = result.getString("AdultPN");
			String adultWorkNumber = result.getString("AdultWN");
			String role = result.getString("AdultRole");
			
			Child child = new Child(childSection, childSSN, childFirstName, childLastName);
			Teacher teacher = new Teacher(teacherSSN, teacherFirstName, teacherLastName, teacherEmail, teacherPhoneNumber);
			Adult adult = new Adult(adultSSN, adultFirstName, adultLastName, adultMail, adultPhoneNumber, adultWorkNumber, role);
			
		}
		if(!statement.getConnection().isClosed()){
			statement.close();
		}
		
		return attendanceList;
	}

	@Override
	public List<Attendance> getAttendancesForChild(Connection conn, String socialSecurityNumber) throws SQLException {
		List<Attendance> attendanceList = new LinkedList<Attendance>();
		String query = "SELECT AttendanceId, isAttending, Arrival, ExpectedClockOut, ClockOut, "
				+ "C.SectionId AS ChildSection, C.SocialSecurityNumber AS ChildSSN, C.First_name AS ChildFN, C.last_name AS ChildLN, "
				+ "T.First_name AS TeacherFN, T.Last_name AS TeacherLN, T.Email AS TeacherMail, T.Phone_number AS TeacherPN, T.SocialSecurityNumber AS TeacherSSN, "
				+ "A.SocialSecurityNumber AS AdultSSN, A.First_name AS AdultFN, A.Last_name AS AdultLN, A.Email AS AdultEmail, A.Phone_number AS AdultPN, A.Work_number AS AdultWN, A.Role AS AdultRole "
				+ "FROM Attendance "
				+ "INNER JOIN Child C ON Attendance.ChildSocialSecurityNumber = C.SocialSecurityNumber "
				+ "INNER JOIN Teacher T ON Attendance.TeacherSocialSecurityNumber = T.SocialSecurityNumber "
				+ "INNER JOIN Adult A ON Attendance.PickedUpBySSN = A.SocialSecurityNumber "
				+ "WHERE Attendance.ChildSocialSecurityNumber = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, socialSecurityNumber);
		ResultSet result = statement.executeQuery();
		
		while(result.next()){
			long childSection = result.getLong("ChildSection");
			String childSSN = result.getString("ChildSSN");
			String childFirstName = result.getString("ChildFN");
			String childLastName = result.getString("ChildLN");
			
			String teacherFirstName = result.getString("TeacherFN");
			String teacherLastName = result.getString("TeacherLN");
			String teacherEmail = result.getString("TeacherMail");
			String teacherSSN = result.getString("TeacherSSN");
			String teacherPhoneNumber = result.getString("TeacherPN");
			
			String adultSSN = result.getString("AdultSSN");
			String adultFirstName = result.getString("AdultFN");
			String adultLastName = result.getString("AdultLN");
			String adultMail = result.getString("AdultEmail");
			String adultPhoneNumber = result.getString("AdultPN");
			String adultWorkNumber = result.getString("AdultWN");
			String role = result.getString("AdultRole");
			
			Child child = new Child(childSection, childSSN, childFirstName, childLastName);
			Teacher teacher = new Teacher(teacherSSN, teacherFirstName, teacherLastName, teacherEmail, teacherPhoneNumber);
			Adult adult = new Adult(adultSSN, adultFirstName, adultLastName, adultMail, adultPhoneNumber, adultWorkNumber, role);
		}
		if(!statement.getConnection().isClosed()){
			statement.close();
		}
		
		return attendanceList;
	}
	
	@Override
	public void postAttendance(Connection conn, Attendance attendance) throws SQLException {
		String sql = "INSERT INTO Attendance(ChildSocialSecurityNumber, TeacherSocialSecurityNumber, PickedUpBySSN, AttendanceDate, Present) VALUES (?, ?, ?, ?, ?)";
		PreparedStatement preparedStatement = conn.prepareStatement(sql);
		preparedStatement.setString(1, attendance.getChild().getSocialSecurityNumber());
		preparedStatement.setString(2, attendance.getTeacher().getSocialSecurityNumber());
		preparedStatement.setString(3, attendance.getAdult().getSocialSecurityNumber());
		preparedStatement.setString(4, attendance.getAttendanceDate());
		preparedStatement.setBoolean(5, true);
		preparedStatement.executeUpdate();

		if (!preparedStatement.getConnection().isClosed()) {
			preparedStatement.close();
		}
	}

	@Override
	public void clockOut(Connection conn, String ssn, String date, ClockOut clockOut) throws SQLException {
		String sql = "UPDATE Present SET ClockOut = ? WHERE C_SSN = ? AND P_Date = ?";
		
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1, clockOut.getTime());
		statement.setString(2, ssn);
		statement.setString(3, date);
		statement.executeUpdate();
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
	}

	/*
	@Override
	public List<Child> getOverview(Connection conn, String date, String section) throws SQLException {
		List<Child> overview = new ArrayList<Child>();
		String query = "SELECT AttendanceDate, Present, "
				+ "C.SocialSecurityNumber AS CSSN, C.First_name AS CFN, C.last_name AS CLN, "
				+ "P.Arrival, P.ClockOut, "
				+ "S.Section_name "
				+ "FROM Attendance "
				+ "INNER JOIN Child C ON Attendance.ChildSocialSecurityNumber = C.SocialSecurityNumber "
				+ "LEFT JOIN Present P ON Attendance.ChildSocialSecurityNumber = P.C_SSN "
				+ "INNER JOIN Section S ON C.SectionId = S.SectionId "
				+ "WHERE AttendanceDate = ? AND S.Section_name = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, date);
		statement.setString(2, section);
		ResultSet result = statement.executeQuery();
		
		while(result.next()) {
			Child child = new Child(result.getString("CSSN"), result.getString("CFN"), result.getString("CLN"), result.getString("Arrival"), result.getString("ClockOut"));
		}
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
		
		return overview;
	}
	*/

	@Override
	public List<Child> getDueToArriveChildren(Connection conn, String section, String date) throws SQLException {
		List<Child> children = new ArrayList<Child>();
		String query = "SELECT C.ChildId AS CID, C.SectionId AS CSID, C.First_name AS CFN, C.Last_name AS CLN, C.SocialSecurityNumber AS CSSN "
				+ "FROM Child C "
				+ "INNER JOIN Section S ON S.SectionId = C.SectionId "
				+ "WHERE NOT EXISTS "
				+ "  (SELECT P.C_SSN, S.Section_name "
				+ "    FROM Present P "
				+ "    WHERE P.C_SSN = C.SocialSecurityNumber AND P_Date = ? AND S.Section_name = ?) "
				+ "AND S.Section_name = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, date);
		statement.setString(2, section);
		statement.setString(3, section);
		ResultSet result = statement.executeQuery();
		
		while(result.next()) {
			children.add(new Child(result.getLong("CID"), result.getLong("CSID"), result.getString("CSSN"), 
					result.getString("CFN"), result.getString("CLN")));
		}
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
		if(!result.isClosed()) {
			result.close();
		}
		
		return children;
	}

	@Override
	public List<Child> getPresentChildren(Connection conn, String section, String date) throws SQLException {
		List<Child> children = new ArrayList<Child>();
		String query = "SELECT C.ChildId AS CID, C.SectionId AS CSID, C.First_name AS CFN, C.Last_name AS CLN, C.SocialSecurityNumber AS CSSN, "
				+ "P.Arrival AS Arrival, P.ClockOut AS ClockOut "
				+ "FROM Child C "
				+ "INNER JOIN Present P ON P.C_SSN = C.SocialSecurityNumber "
				+ "INNER JOIN Section S ON S.SectionId = C.SectionId "
				+ "WHERE EXISTS "
				+ "(SELECT P.C_SSN, S.Section_name "
				+ "FROM Present P "
				+ "WHERE P.C_SSN = C.SocialSecurityNumber AND P_Date = ? AND S.Section_name = ?) "
				+ "AND S.Section_name = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, date);
		statement.setString(2, section);
		statement.setString(3, section);
		ResultSet result = statement.executeQuery();
		
		while(result.next()) {
			children.add(new Child(result.getLong("CID"), result.getLong("CSID"), result.getString("CSSN"), 
					result.getString("CFN"), result.getString("CLN"), result.getString("Arrival"), result.getString("ClockOut")));
		}
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
		if(!result.isClosed()) {
			result.close();
		}
		
		return children;
	}
	
	/**
	 * THIS MAY NEED SOME REFACTOR ON THE PK
	 */
	@Override
	public void clockIn(Connection conn, Present present) throws SQLException {
		String query = "SELECT COUNT(*) AS maxrows FROM Present";
		
		Statement statement = conn.createStatement();
		ResultSet result = statement.executeQuery(query);
		
		if(result.next()) {
			long presentId = result.getLong("maxrows");
			String sql = "INSERT INTO Present(P_ID, C_SSN, P_Date, Arrival) VALUES (?, ?, ?, ?)";
			
			PreparedStatement pStatement = conn.prepareStatement(sql);
			pStatement.setLong(1, presentId + 1);
			pStatement.setString(2, present.getChild().getSocialSecurityNumber());
			pStatement.setString(3, present.getDate());
			pStatement.setString(4, present.getArrival());
			pStatement.executeUpdate();
			
			if(!pStatement.getConnection().isClosed()) {
				pStatement.close();
			}
		}
		result.close();
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
	}

	@Override
	public void postOrUpdateChildMessage(Connection conn, ChildMessage message) throws SQLException {
		String query = "SELECT * FROM Child_Message WHERE CSSN = ?";
		int executedUpdate = 0;
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, message.getChild().getSocialSecurityNumber());
		ResultSet result = statement.executeQuery();
		
		if(!result.first()) {
			String sql = "INSERT INTO Child_Message(CSSN, Message) VALUES (?, ?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, message.getChild().getSocialSecurityNumber());
			pStmt.setString(2, message.getMessage());
			executedUpdate = pStmt.executeUpdate();
			if(executedUpdate > 0 && !pStmt.getConnection().isClosed()) {
				System.out.println("INSERT successful & closing statement");
				pStmt.close();
			}
		}
		else if(result.first()) {
			String sql = "UPDATE Child_Message SET Message = ? WHERE CSSN = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, message.getMessage());
			stmt.setString(2, message.getChild().getSocialSecurityNumber());
			executedUpdate = stmt.executeUpdate();
			if(executedUpdate > 0 && !stmt.getConnection().isClosed()) {
				System.out.println("UPDATE successful");
				stmt.close();
			}
		}
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
		if(!result.isClosed()) {
			result.close();
		}
	}

	@Override
	public ChildMessage getMessage(Connection conn, String socialSecurityNumber) throws SQLException {
		String query = "SELECT Message FROM Child_Message WHERE CSSN = ?";
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, socialSecurityNumber);
		ResultSet result = statement.executeQuery();
		
		if(result.next()) {
			return new ChildMessage(result.getString("Message"));
		}
		else {
			return null;
		}
	}
}
