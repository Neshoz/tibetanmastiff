package newton.se.TibetanMastiff.service.login;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import newton.se.TibetanMastiff.model.UserToken;

/*
 * DataAcceslayer for Login Validation.
 * */
public class LoginServiceImpl implements LoginService {

	@Override
	public boolean validateCredential(Connection conn, UserToken token) throws SQLException {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		UserToken dbUserToken = null;
		boolean validated = false;

		String query = "SELECT Username,Pass FROM admin WHERE Username = ?";

		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, token.getUsername());
		ResultSet result = statement.executeQuery();

		if (result.next()) {
			dbUserToken = new UserToken(result.getString("Username"), result.getString("Pass"));

			if (dbUserToken.getUsername().equals(token.getUsername())
					&& (!dbUserToken.getPassword().equals(token.getPassword()))) {
				System.out.println(timestamp + " Login Fail: Password does not match.");
				validated = false;

			} else {
				System.out.println(timestamp + " Login Sucess: " + dbUserToken.getUsername());
				validated = true;
			}

		} else {
			System.out.println(timestamp + " Login Fail: Account does not exist.");
			validated = false;
		}

		if (!statement.getConnection().isClosed()) {
			statement.close();
		}
		return validated;
	}

}
