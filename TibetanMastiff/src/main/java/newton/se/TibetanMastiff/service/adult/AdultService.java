package newton.se.TibetanMastiff.service.adult;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import newton.se.TibetanMastiff.model.Adult;

public interface AdultService {
	List<Adult> getAllAdults(Connection conn) throws SQLException;
	Adult getAdult(Connection conn, String SocialSecurityNumber) throws SQLException;
	void postAdult(Connection conn, String childSSN, Adult adult) throws SQLException;
	void deleteAdult(Connection conn,Adult adult) throws SQLException;
	void updateAdultInformation(Connection conn, String socialSecurityNumber, Adult adult) throws SQLException;
}
