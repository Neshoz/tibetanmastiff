package newton.se.TibetanMastiff.service.login;

import java.sql.Connection;
import java.sql.SQLException;

import newton.se.TibetanMastiff.model.UserToken;

public interface LoginService {

	boolean validateCredential(Connection conn, UserToken token) throws SQLException;
}
