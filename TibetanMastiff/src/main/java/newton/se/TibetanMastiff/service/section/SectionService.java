package newton.se.TibetanMastiff.service.section;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import newton.se.TibetanMastiff.model.Section;

public interface SectionService {
	List<Section> getAllSections(Connection conn) throws SQLException;
	
	Section getSection(Connection conn, String sectionName) throws SQLException;
	
	void postSection(Connection conn, Section section) throws SQLException;
	void updateSection(Connection conn, Section section) throws SQLException;
}
