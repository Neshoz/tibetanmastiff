package newton.se.TibetanMastiff.service.attendance;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import newton.se.TibetanMastiff.model.Attendance;
import newton.se.TibetanMastiff.model.AttendanceOverview;
import newton.se.TibetanMastiff.model.Child;
import newton.se.TibetanMastiff.model.ChildMessage;
import newton.se.TibetanMastiff.model.ClockOut;
import newton.se.TibetanMastiff.model.Present;

public interface AttendanceService {
	List<Attendance> getAllAttendances(Connection conn) throws SQLException;
	List<Attendance> getAttendancesForChild(Connection conn, String socialSecurityNumber) throws SQLException;
	
	//List<Child> getOverview(Connection conn, String date, String section) throws SQLException;
	List<Child> getDueToArriveChildren(Connection conn, String section, String date) throws SQLException;
	List<Child> getPresentChildren(Connection conn, String section, String date) throws SQLException;
	
	ChildMessage getMessage(Connection conn, String socialSecurityNumber) throws SQLException;
	
	void postAttendance(Connection conn, Attendance attendance) throws SQLException;
	void clockOut(Connection conn, String ssn, String date, ClockOut clockOut) throws SQLException;
	void clockIn(Connection conn, Present present) throws SQLException;
	void postOrUpdateChildMessage(Connection conn, ChildMessage message) throws SQLException;
}