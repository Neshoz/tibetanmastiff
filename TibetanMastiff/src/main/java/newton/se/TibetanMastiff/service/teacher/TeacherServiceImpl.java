package newton.se.TibetanMastiff.service.teacher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import newton.se.TibetanMastiff.model.Teacher;

/*
 * DataAcceslayer for Teacher.
 * */
public class TeacherServiceImpl implements TeacherService {

	@Override
	public List<Teacher> getAllTeachers(Connection conn) throws SQLException {
		List<Teacher> teachers = new LinkedList<Teacher>();
		String query = "SELECT * FROM Teacher";

		Statement statement = conn.createStatement();
		ResultSet result = statement.executeQuery(query);

		while (result.next()) {
			teachers.add(new Teacher(result.getString("SocialSecurityNumber"), result.getString("First_name"),
					result.getString("Last_name"), result.getString("Email"), result.getString("Phone_number")));
		}
		if (!statement.getConnection().isClosed()) {
			statement.close();
		}

		return teachers;
	}

	@Override
	public Teacher getTeacher(Connection conn, String socialSecurityNumber) throws SQLException {
		String query = "SELECT * FROM Teacher WHERE SocialSecurityNumber = ?";
		Teacher teacher = null;

		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, socialSecurityNumber);
		ResultSet result = statement.executeQuery();

		if (result.next()) {
			teacher = new Teacher(result.getString("SocialSecurityNumber"), result.getString("First_name"),
					result.getString("Last_name"), result.getString("Email"), result.getString("Phone_number"));
		}
		if (!statement.getConnection().isClosed()) {
			statement.close();
		}
		return teacher;
	}

	@Override
	public void postTeacher(Connection conn, Teacher teacher) throws SQLException {
		String query = "INSERT INTO Teacher(SocialSecurityNumber, First_name, Last_name, Email, Phone_Number) VALUES (?,?,?,?,?)";

		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, teacher.getSocialSecurityNumber());
		statement.setString(2, teacher.getFirstName());
		statement.setString(3, teacher.getLastName());
		statement.setString(4, teacher.getEmail());
		statement.setString(5, teacher.getPhoneNumber());
		statement.executeUpdate();

		if (!statement.getConnection().isClosed()) {
			statement.close();
		}
	}

	@Override
	public void deleteTeacher(Connection conn, Teacher teacher) throws SQLException {
		String query = "DELETE FROM Teacher WHERE SocialSecurityNumber = ?";
		PreparedStatement statement = conn.prepareStatement(query);

		statement.setString(1, teacher.getSocialSecurityNumber());
		statement.executeUpdate();

		if (!statement.getConnection().isClosed()) {
			statement.close();
		}
	}

	@Override
	public void updateTeacher(Connection conn, Teacher teacher) throws SQLException {
		
	}

}
