
package newton.se.TibetanMastiff.service.present;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import newton.se.TibetanMastiff.model.Adult;
import newton.se.TibetanMastiff.model.Child;
import newton.se.TibetanMastiff.model.Present;
import newton.se.TibetanMastiff.model.Section;


public class PresentServiceImpl implements PresentService {
	

	@Override
	public List<Present> getAllPresentChildren(Connection conn, String date) throws SQLException {
		List<Present> presentChildren = new ArrayList<Present>();
		String query = "SELECT P_ID, P_Date, Arrival, ClockOut "
				+ "C.SocialSecurityNumber AS CSSN, C.First_name AS CFN, C.Last_name AS CLN, "
				+ "S.SectionId AS SID, S.Section_name AS SName, "
				+ "AD.AdultId AS ADId, AD.SocialSecurityNumber AS ADSSN, AD.First_name AS ADFN, AD.Last_name AS ADLN, AD.Email AS ADEmail, AD.Phone_number AS ADPN, AD.Work_number AS ADWN, AD.Role as ADRole, "
				+ "AM.AdultId AS AMId, AM.SocialSecurityNumber AS AMSSN, AM.First_name AS AMFN, AD.Last_name AS AMLN, AM.Email AS AMEmail, AM.Phone_number AS AMPN, AM.Work_number AS AMWN, AM.Role AS AMRole "
				+ "FROM Present "
				+ "INNER JOIN Child C ON Present.C_SSN = C.SocialSecurityNumber "
				+ "INNER JOIN Section S ON C.SectionId = S.SectionId "
				+ "INNER JOIN Adult AD ON C.FatherSocialSecurityNumber = AD.SocialSecurityNumber "
				+ "INNER JOIN Adult AM ON C.MotherSocialSecurityNumber = AM.SocialSecurityNumber "
				+ "WHERE P_Date = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, date);
		ResultSet result = statement.executeQuery();
		
		while(result.next()) {
			/**
			 * Present variables
			 */
			long presentId = result.getLong("P_ID");
			String presentDate = result.getString("P_Date");
			String arrival = result.getString("Arrival");
			String clockOut = result.getString("ClockOut");
			
			/**
			 * Child variables
			 */
			String childSSN = result.getString("CSSN");
			String childFN = result.getString("CFN");
			String childLN = result.getString("CLN");
			
			long sectionId = result.getLong("SID");
			String sectionName = result.getString("sName");
			
			long adId = result.getLong("ADId");
			String adSSN = result.getString("ADSSN");
			String adPN = result.getString("ADPN");
			String adWN = result.getString("ADWN");
			String adFN = result.getString("ADFN");
			String adLN = result.getString("ADLN");
			String adEmail = result.getString("ADEmail");
			String adRole = result.getString("ADRole");
			Adult father = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
			
			adId = result.getLong("AMId");
			adSSN = result.getString("AMSSN");
			adPN = result.getString("AMPN");
			adWN = result.getString("AMWN");
			adFN = result.getString("AMFN");
			adLN = result.getString("AMLN");
			adEmail = result.getString("AMEmail");
			adRole = result.getString("AMRole");
			Adult mother = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
			
			Section section = new Section(sectionId, sectionName);
			Child child = new Child(sectionId, childSSN, childFN, childLN);
			
			child.getParents().add(father);
			child.getParents().add(mother);
			
			//presentChildren.add(new Present(presentId, section, child, presentDate, arrival, clockOut));
		}
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
		
		return presentChildren;
	}

	@Override
	public List<Present> getPresentChildrenInSection(Connection conn, String querySection, String date) throws SQLException {
		List<Present> presentChildren = new ArrayList<Present>();
		String query = "SELECT P_ID, P_Date, Arrival, ClockOut, "
				+ "C.SocialSecurityNumber AS CSSN, C.First_name AS CFN, C.Last_name AS CLN, "
				+ "S.SectionId AS SId, S.Section_name AS SName, "
				+ "AD.AdultId AS ADId, AD.SocialSecurityNumber AS ADSSN, AD.First_name AS ADFN, AD.Last_name AS ADLN, AD.Email AS ADEmail, AD.Phone_number AS ADPN, AD.Work_number AS ADWN, AD.Role as ADRole, "
				+ "AM.AdultId AS AMId, AM.SocialSecurityNumber AS AMSSN, AM.First_name AS AMFN, AD.Last_name AS AMLN, AM.Email AS AMEmail, AM.Phone_number AS AMPN, AM.Work_number AS AMWN, AM.Role AS AMRole "
				+ "FROM Present "
				+ "INNER JOIN Child C ON Present.C_SSN = C.SocialSecurityNumber "
				+ "INNER JOIN Section S ON C.SectionId = S.SectionId "
				+ "INNER JOIN Adult AD ON C.FatherSocialSecurityNumber = AD.SocialSecurityNumber "
				+ "INNER JOIN Adult AM ON C.MotherSocialSecurityNumber = AM.SocialSecurityNumber "
				+ "WHERE S.Section_name = ? AND Present.P_Date = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, querySection);
		statement.setString(2, date);
		ResultSet result = statement.executeQuery();
		
		while(result.next()) {
			if(result.getString("ClockOut") != null) {
				result.next();
			}
			else{
				/**
				 * Present variables
				 */
				long presentId = result.getLong("P_ID");
				String presentDate = result.getString("P_Date");
				String arrival = result.getString("Arrival");
				String clockOut = result.getString("ClockOut");
				
				/**
				 * Child variables
				 */
				String childSSN = result.getString("CSSN");
				String childFN = result.getString("CFN");
				String childLN = result.getString("CLN");
				
				long sectionId = result.getLong("SID");
				String sectionName = result.getString("sName");
				
				long adId = result.getLong("ADId");
				String adSSN = result.getString("ADSSN");
				String adPN = result.getString("ADPN");
				String adWN = result.getString("ADWN");
				String adFN = result.getString("ADFN");
				String adLN = result.getString("ADLN");
				String adEmail = result.getString("ADEmail");
				String adRole = result.getString("ADRole");
				Adult father = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
				
				adId = result.getLong("AMId");
				adSSN = result.getString("AMSSN");
				adPN = result.getString("AMPN");
				adWN = result.getString("AMWN");
				adFN = result.getString("AMFN");
				adLN = result.getString("AMLN");
				adEmail = result.getString("AMEmail");
				adRole = result.getString("AMRole");
				Adult mother = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
				
				Section section = new Section(sectionId, sectionName);
				Child child = new Child(sectionId, childSSN, childFN, childLN);
				
				child.getParents().add(father);
				child.getParents().add(mother);
				
				//presentChildren.add(new Present(presentId, section, child, presentDate, arrival, clockOut));
			}
		}
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
		
		return presentChildren;
	}

	@Override
	public Present getPresentChild(Connection conn, String socialSecurityNumber, String date, String querySection) throws SQLException {
		Present present = null;
		String query = "SELECT P_ID, P_Date, Arrival, ClockOut, "
				+ "C.SocialSecurityNumber AS CSSN, C.First_name AS CFN, C.Last_name AS CLN, "
				+ "S.SectionId AS SId, S.Section_name AS SName, "
				+ "AD.AdultId AS ADId, AD.SocialSecurityNumber AS ADSSN, AD.First_name AS ADFN, AD.Last_name AS ADLN, AD.Email AS ADEmail, AD.Phone_number AS ADPN, AD.Work_number AS ADWN, AD.Role as ADRole, "
				+ "AM.AdultId AS AMId, AM.SocialSecurityNumber AS AMSSN, AM.First_name AS AMFN, AD.Last_name AS AMLN, AM.Email AS AMEmail, AM.Phone_number AS AMPN, AM.Work_number AS AMWN, AM.Role AS AMRole "
				+ "FROM Present "
				+ "INNER JOIN Child C ON Present.C_SSN = C.SocialSecurityNumber "
				+ "INNER JOIN Section S ON C.SectionId = S.SectionId "
				+ "INNER JOIN Adult AD ON C.FatherSocialSecurityNumber = AD.SocialSecurityNumber "
				+ "INNER JOIN Adult AM ON C.MotherSocialSecurityNumber = AM.SocialSecurityNumber "
				+ "WHERE Present.P_Date = ? AND Present.C_SSN = ? AND S.Section_name = ?";
		
		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, date);
		statement.setString(2, socialSecurityNumber);
		statement.setString(3, querySection);
		ResultSet result = statement.executeQuery();
		
		if(result.next()) {
			/**
			 * Present variables
			 */
			long presentId = result.getLong("P_ID");
			String presentDate = result.getString("P_Date");
			String arrival = result.getString("Arrival");
			String clockOut = result.getString("ClockOut");
			
			/**
			 * Child variables
			 */
			String childSSN = result.getString("CSSN");
			String childFN = result.getString("CFN");
			String childLN = result.getString("CLN");
			
			long sectionId = result.getLong("SID");
			String sectionName = result.getString("sName");
			
			long adId = result.getLong("ADId");
			String adSSN = result.getString("ADSSN");
			String adPN = result.getString("ADPN");
			String adWN = result.getString("ADWN");
			String adFN = result.getString("ADFN");
			String adLN = result.getString("ADLN");
			String adEmail = result.getString("ADEmail");
			String adRole = result.getString("ADRole");
			Adult father = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
			
			adId = result.getLong("AMId");
			adSSN = result.getString("AMSSN");
			adPN = result.getString("AMPN");
			adWN = result.getString("AMWN");
			adFN = result.getString("AMFN");
			adLN = result.getString("AMLN");
			adEmail = result.getString("AMEmail");
			adRole = result.getString("AMRole");
			Adult mother = new Adult(adSSN, adFN, adLN, adEmail, adPN, adWN, adRole);
			
			Section section = new Section(sectionId, sectionName);
			Child child = new Child(sectionId, childSSN, childFN, childLN);
			
			child.getParents().add(father);
			child.getParents().add(mother);
			
			//present = new Present(presentId, section, child, presentDate, arrival, clockOut);
		}
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
		
		return present;
	}

	@Override
	public void postPresentAttendance(Connection conn, Present present) throws SQLException {
		String sql = "INSERT INTO Present(C_SSN, P_Date, Arrival) VALUES (?, ?, ?)";

		PreparedStatement pStatement = conn.prepareStatement(sql);
		pStatement.setString(1, present.getChild().getSocialSecurityNumber());
		pStatement.setString(2, present.getDate());
		pStatement.setString(3, present.getArrival());
		pStatement.executeUpdate();

		if (!pStatement.getConnection().isClosed()) {
			pStatement.close();
		}
	}

	@Override
	public void clockOutPresentChild(Connection conn, String socialSecurityNumber, String date, String clockOutTime) throws SQLException {
		String sql = "UPDATE Present SET ClockOut = ? WHERE C_SSN = ? AND P_Date = ?";
		
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1, clockOutTime);
		statement.setString(2, socialSecurityNumber);
		statement.setString(3, date);
		statement.executeUpdate();
		
		if(!statement.getConnection().isClosed()) {
			statement.close();
		}
	}

}
