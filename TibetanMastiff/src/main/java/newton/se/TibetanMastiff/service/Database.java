package newton.se.TibetanMastiff.service;

import java.sql.Connection;
import java.sql.DriverManager;

public class Database {
	public Connection getConnection() throws Exception{
		try{
			String connectionURL = "jdbc:mysql://localhost:3306/ExamDB";
			String connectionUsername = "root";
			String connectionPassword = "root";
			Connection connection = null;
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connection = DriverManager.getConnection(connectionURL, connectionUsername, connectionPassword);
			if(connection != null){
				System.out.println("Connection successful");
			}
			else{
				System.out.println("Connection failed");
			}
			return connection;
		}
		catch (Exception ex){
			throw ex;
		}
	}
}
