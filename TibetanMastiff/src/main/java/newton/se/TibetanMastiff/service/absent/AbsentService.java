package newton.se.TibetanMastiff.service.absent;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import newton.se.TibetanMastiff.model.Absent;
import newton.se.TibetanMastiff.model.Child;

public interface AbsentService {
	List<Absent> getAllAbsentChildren(Connection conn, String date) throws SQLException;
	List<Absent> getAbsentChildrenInSection(Connection conn, String date, String section) throws SQLException;
	
	Absent getAbsentChild(Connection conn, String socialSecurityNumber, String date, String querySection) throws SQLException;
	
	void postAbsentAttendance(Connection conn, Absent absent) throws SQLException;
}
