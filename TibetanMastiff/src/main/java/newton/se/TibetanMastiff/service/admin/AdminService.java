package newton.se.TibetanMastiff.service.admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import newton.se.TibetanMastiff.model.Admin;

public interface AdminService {
	List<Admin> getAllAdmins(Connection conn) throws SQLException;

	Admin getAdmin(Connection conn, String username) throws SQLException;

	void postAdmin(Connection conn, Admin admin) throws SQLException;
	void updateAdmin(Connection conn, Admin admin) throws SQLException;
	void deleteAdmin(Connection conn, Admin admin) throws SQLException;
}
