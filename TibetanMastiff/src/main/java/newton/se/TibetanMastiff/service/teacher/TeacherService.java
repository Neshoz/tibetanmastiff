package newton.se.TibetanMastiff.service.teacher;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import newton.se.TibetanMastiff.model.Teacher;

public interface TeacherService {
	List<Teacher> getAllTeachers(Connection conn) throws SQLException;
	
	Teacher getTeacher(Connection conn, String SocialSecurityNumber) throws SQLException;
	
	void postTeacher(Connection conn, Teacher teacher) throws SQLException;
	void deleteTeacher(Connection conn, Teacher teacher) throws SQLException;
	void updateTeacher(Connection conn, Teacher teacher) throws SQLException;
}
