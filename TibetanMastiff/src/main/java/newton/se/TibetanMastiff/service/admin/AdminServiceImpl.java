package newton.se.TibetanMastiff.service.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import newton.se.TibetanMastiff.model.Admin;

public class AdminServiceImpl implements AdminService {

	@Override
	public List<Admin> getAllAdmins(Connection conn) throws SQLException {
		List<Admin> admins = new LinkedList<Admin>();
		String query = "SELECT * FROM Admin";

		Statement statement = conn.createStatement();
		ResultSet result = statement.executeQuery(query);

		while (result.next()) {
			admins.add(new Admin(result.getString("First_name"), result.getString("Last_name"),
					result.getString("Username"), result.getString("Pass"), result.getString("Email")));
		}
		if (!statement.getConnection().isClosed()) {
			statement.close();
		}

		return admins;
	}

	@Override
	public Admin getAdmin(Connection conn, String username) throws SQLException {
		String query = "SELECT * FROM Admin WHERE Username = ?";
		Admin admin = null;

		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, username);
		ResultSet result = statement.executeQuery();

		if (result.next()) {
			admin = new Admin(result.getString("First_name"), result.getString("Last_name"),
					result.getString("Username"), result.getString("Pass"), result.getString("Email"));
		}
		if (!statement.getConnection().isClosed()) {
			statement.close();
		}
		return admin;
	}

	@Override
	public void postAdmin(Connection conn, Admin admin) throws SQLException {
		String query = "INSERT INTO Admin(First_name, Last_name, Username, Pass, Email) VALUES (?,?,?,?,?)";

		PreparedStatement statement = conn.prepareStatement(query);
		statement.setString(1, admin.getFirstName());
		statement.setString(2, admin.getLastName());
		statement.setString(3, admin.getUserName());
		statement.setString(4, admin.getPassword());
		statement.setString(5, admin.getEmail());
		statement.executeUpdate();

		if (!statement.getConnection().isClosed()) {
			statement.close();
		}
	}

	@Override
	public void updateAdmin(Connection conn, Admin admin) throws SQLException {
		int executedUpdate = 0;
		String sql = "UPDATE ADMIN SET First_name = ?, Last_name = ?, Username = ?, Pass = ?, Email = ? WHERE Email = ?";
		
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1, admin.getFirstName());
		statement.setString(2, admin.getLastName());
		statement.setString(3, admin.getUserName());
		statement.setString(4, admin.getPassword());
		statement.setString(5, admin.getEmail());
		statement.setString(6, admin.getEmail());
		executedUpdate = statement.executeUpdate();
		
		if(executedUpdate > 0 && !statement.getConnection().isClosed()) {
			statement.getConnection().close();
		}
	}

	@Override
	public void deleteAdmin(Connection conn, Admin admin) throws SQLException {
		int executedUpdate = 0;
		String sql = "DELETE FROM Admin WHERE Email = ?";
		
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.setString(1, admin.getEmail());
		executedUpdate = statement.executeUpdate();
		
		if(executedUpdate > 0 && !statement.getConnection().isClosed()) {
			statement.getConnection().close();
		}
	}

}
