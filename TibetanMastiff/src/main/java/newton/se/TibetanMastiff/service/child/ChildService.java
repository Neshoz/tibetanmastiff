package newton.se.TibetanMastiff.service.child;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import newton.se.TibetanMastiff.model.Adult;
import newton.se.TibetanMastiff.model.Child;

public interface ChildService {
	List<Child> getAllChildren(Connection conn) throws SQLException;
	List<Child> getAllChildrenInSection(Connection conn, String sectionName) throws SQLException;
	List<Adult> getAllowedPickUpAdults(Connection conn, String socialSecurityNumber) throws SQLException;
	
	Child getChild(Connection conn, String socialSecurityNumber) throws SQLException;
	
	void postChild(Connection conn, String sectionName, Child child) throws SQLException;
	void deleteChild(Connection conn, Child child) throws SQLException;
	void updateChild(Connection conn, Child child) throws SQLException;
}
