package newton.se.TibetanMastiff.resource;

import java.sql.Connection;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import newton.se.TibetanMastiff.model.UserToken;
import newton.se.TibetanMastiff.service.Database;
import newton.se.TibetanMastiff.service.login.LoginService;
import newton.se.TibetanMastiff.service.login.LoginServiceImpl;

/*
 * Login validation - End point for accessing Rest.
 * */
@Path("/validation")
public class LoginResource {
	private LoginService loginService = new LoginServiceImpl();

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean validate(UserToken token) {
		boolean validated = false;

		try {
			Connection conn = new Database().getConnection();
			validated = loginService.validateCredential(conn, token);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}

		return validated;
	}

}
