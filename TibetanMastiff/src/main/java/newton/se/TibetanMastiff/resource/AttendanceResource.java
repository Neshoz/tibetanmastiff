package newton.se.TibetanMastiff.resource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import newton.se.TibetanMastiff.model.Attendance;
import newton.se.TibetanMastiff.model.Child;
import newton.se.TibetanMastiff.model.ChildMessage;
import newton.se.TibetanMastiff.model.ClockOut;
import newton.se.TibetanMastiff.model.Present;
import newton.se.TibetanMastiff.service.Database;
import newton.se.TibetanMastiff.service.attendance.AttendanceService;
import newton.se.TibetanMastiff.service.attendance.AttendanceServiceImpl;

@Path("/attendance")
public class AttendanceResource {
	private AttendanceService attendanceService = new AttendanceServiceImpl();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Attendance> getAllAttendances(){
		List<Attendance> attendanceList = new ArrayList<Attendance>();
		
		try{
			Connection conn = new Database().getConnection();
			attendanceList = attendanceService.getAllAttendances(conn);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return attendanceList;
	}
	
	/*
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/overview/{date}/{section}")
	public List<Child> getOverview(@PathParam("date") String date, @PathParam("section") String section) {
		List<Child> overview = new ArrayList<Child>();
		
		try{
			Connection conn = new Database().getConnection();
			overview = attendanceService.getOverview(conn, date, section);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return overview;
	}
	*/
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{section}/{date}/absent")
	public List<Child> getDueToArriveChildren(@PathParam("section") String section, @PathParam("date") String date) {
		List<Child> data = null;
		
		try{
			Connection conn = new Database().getConnection();
			data = attendanceService.getDueToArriveChildren(conn, section, date);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return data;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{section}/{date}/present")
	public List<Child> getPresentChildren(@PathParam("section") String section, @PathParam("date") String date) {
		List<Child> data = null;
		
		try {
			Connection conn = new Database().getConnection();
			data = attendanceService.getPresentChildren(conn, section, date);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return data;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{socialSecurityNumber}")
	public List<Attendance> getAttendanceForChild(@PathParam("socialSecurityNumber") String socialSecurityNumber){
		List<Attendance> attendanceList = new LinkedList<Attendance>();
		
		try{
			Connection conn = new Database().getConnection();
			attendanceList = attendanceService.getAttendancesForChild(conn, socialSecurityNumber);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return attendanceList;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{ssn}/message")
	public ChildMessage getMessage(@PathParam("ssn") String socialSecurityNumber) {
		ChildMessage message = null;
		
		try {
			Connection conn = new Database().getConnection();
			message = attendanceService.getMessage(conn, socialSecurityNumber);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return message;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/clockin")
	public void clockIn(Present present) {
		try {
			Connection conn = new Database().getConnection();
			attendanceService.clockIn(conn, present);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{ssn}/{date}/clockout")
	public void clockOut(@PathParam("ssn") String ssn, @PathParam("date") String date, ClockOut clockOut) {
		try {
			Connection conn = new Database().getConnection();
			attendanceService.clockOut(conn, ssn, date, clockOut);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/post")
	public void postAttendance(Attendance attendance) {
		try {
			Connection conn = new Database().getConnection();
			attendanceService.postAttendance(conn, attendance);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/message")
	public void postOrUpdateMessage(ChildMessage message) {
		try {
			Connection conn = new Database().getConnection();
			attendanceService.postOrUpdateChildMessage(conn, message);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
}
