package newton.se.TibetanMastiff.resource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import newton.se.TibetanMastiff.model.Emergency;
import newton.se.TibetanMastiff.service.Database;
import newton.se.TibetanMastiff.service.emergency.EmergencyService;
import newton.se.TibetanMastiff.service.emergency.EmergencyServiceImpl;

@Path("/emergencies")
public class EmergencyResource {
	
	private EmergencyService emergencyService = new EmergencyServiceImpl();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Emergency> getAllEmergencies(){
		List<Emergency> emergencyList = new LinkedList<Emergency>();
		
		try{
			Connection conn = new Database().getConnection();
			emergencyList = emergencyService.getAllEmergencies(conn);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return emergencyList;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Emergency getEmergency(@PathParam("id") int emergencyId){
		Emergency emergency = null;
		
		try{
			Connection conn = new Database().getConnection();
			emergency = emergencyService.getEmergency(conn, emergencyId);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return emergency;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void postEmergency(Emergency emergency){
		try{
			Connection conn = new Database().getConnection();
			emergencyService.postEmergency(conn, emergency);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
}
