package newton.se.TibetanMastiff.resource;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import newton.se.TibetanMastiff.model.ClockOut;
import newton.se.TibetanMastiff.model.Present;
import newton.se.TibetanMastiff.service.Database;
import newton.se.TibetanMastiff.service.present.PresentService;
import newton.se.TibetanMastiff.service.present.PresentServiceImpl;

@Path("/present")
public class PresentResource {
	
	private PresentService service = new PresentServiceImpl();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{date}")
	public List<Present> getAllPresentChildren(@PathParam("date") String date) {
		List<Present> presentChildren = null;
		
		try{
			Connection conn = new Database().getConnection();
			presentChildren = service.getAllPresentChildren(conn, date);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return presentChildren;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{date}/{section}")
	public List<Present> getPresentChildrenInSection(@PathParam("date") String date, @PathParam("section") String section) {
		List<Present> presentInSection = null;
		
		try{
			Connection conn = new Database().getConnection();
			presentInSection = service.getPresentChildrenInSection(conn, section, date);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return presentInSection;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{date}/{section}/{ssn}")
	public Present getPresentChild(@PathParam("date") String date, @PathParam("section") String section, @PathParam("ssn") String socialSecurityNumber) {
		Present presentChild = null;
		
		try{
			Connection conn = new Database().getConnection();
			presentChild = service.getPresentChild(conn, socialSecurityNumber, date, section);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return presentChild;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void postPresentAttendance(Present present) {
		try{
			Connection conn = new Database().getConnection();
			service.postPresentAttendance(conn, present);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
	
	@POST
	@Path("/{date}/{ssn}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void clockOutPresentChild(@PathParam("date") String date, @PathParam("ssn") String socialSecurityNumber, ClockOut clockOutTime) {
		try{
			Connection conn = new Database().getConnection();
			service.clockOutPresentChild(conn, socialSecurityNumber, date, clockOutTime.getTime());
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
}
