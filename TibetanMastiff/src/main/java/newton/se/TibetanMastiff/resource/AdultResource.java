package newton.se.TibetanMastiff.resource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import newton.se.TibetanMastiff.model.Adult;
import newton.se.TibetanMastiff.service.Database;
import newton.se.TibetanMastiff.service.adult.AdultService;
import newton.se.TibetanMastiff.service.adult.AdultServiceImpl;

/*
 * Adult - End point for accessing Rest.
 * */
@Path("/adults")
public class AdultResource {

	private AdultService adultService = new AdultServiceImpl();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Adult> getAllAdults() {
		List<Adult> adults = new LinkedList<Adult>();

		try {
			Connection conn = new Database().getConnection();
			adults = adultService.getAllAdults(conn);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}

		return adults;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{socialSecurityNumber}")
	public Adult getAdult(@PathParam("socialSecurityNumber") String socialSecurityNumber) {
		Adult adult = null;

		try {
			Connection conn = new Database().getConnection();
			adult = adultService.getAdult(conn, socialSecurityNumber);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}

		return adult;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{childSSN}")
	public void postAdult(@PathParam("childSSN") String childSSN, Adult adult) {
		try {
			Connection conn = new Database().getConnection();
			adultService.postAdult(conn, childSSN, adult);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{socialSecurityNumber}/delete")
	public void deleteAdult(@PathParam("socialSecurityNumber") String socialSecurityNumber, Adult adult) {
		try {
			Connection conn = new Database().getConnection();
			adultService.deleteAdult(conn, adult);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{socialSecurityNumber}/updateAdult")
	public void updateAdultInformation(@PathParam("socialSecurityNumber") String socialSecurityNumber, Adult adult) {
		try {
			Connection conn = new Database().getConnection();
			adultService.updateAdultInformation(conn, socialSecurityNumber, adult);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}
	}
}


