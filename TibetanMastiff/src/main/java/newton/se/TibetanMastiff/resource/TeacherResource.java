package newton.se.TibetanMastiff.resource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import newton.se.TibetanMastiff.model.Teacher;
import newton.se.TibetanMastiff.service.Database;
import newton.se.TibetanMastiff.service.teacher.TeacherService;
import newton.se.TibetanMastiff.service.teacher.TeacherServiceImpl;

@Path("/teachers")
public class TeacherResource {

	private TeacherService teacherService = new TeacherServiceImpl();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Teacher> getAllTeachers() {
		List<Teacher> teachers = new LinkedList<Teacher>();

		try {
			Connection conn = new Database().getConnection();
			teachers = teacherService.getAllTeachers(conn);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}

		return teachers;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{socialSecurityNumber}")
	public Teacher getTeacher(@PathParam("socialSecurityNumber") String socialSecurityNumber) {
		Teacher teacher = null;

		try {
			Connection conn = new Database().getConnection();
			teacher = teacherService.getTeacher(conn, socialSecurityNumber);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}

		return teacher;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void postTeacher(Teacher teacher) {
		try {
			Connection conn = new Database().getConnection();
			teacherService.postTeacher(conn, teacher);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/update")
	public void updateTeacher(Teacher teacher) {
		try {
			Connection conn = new Database().getConnection();
			teacherService.updateTeacher(conn, teacher);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{socialSecurityNumber}/delete")
	public void deleteTeacher(@PathParam("socialSecurityNumber") String socialSecurityNumber, Teacher teacher) {
		try {
			Connection conn = new Database().getConnection();
			teacherService.deleteTeacher(conn, teacher);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}
	}
}
