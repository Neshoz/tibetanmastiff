package newton.se.TibetanMastiff.resource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import newton.se.TibetanMastiff.model.Section;
import newton.se.TibetanMastiff.service.Database;
import newton.se.TibetanMastiff.service.section.SectionService;
import newton.se.TibetanMastiff.service.section.SectionServiceImpl;

@Path("/sections")
public class SectionResource {
	
	private SectionService sectionService = new SectionServiceImpl();
	
	@GET
	@Produces("application/json")
	public List<Section> getAllSections(){
		List<Section> sections = new ArrayList<Section>();
		
		try{
			Connection conn = new Database().getConnection();
			sections = sectionService.getAllSections(conn);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return sections;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{sectionName}")
	public Section getSection(@PathParam("sectionName") String sectionName){
		Section section = null;
		
		try{
			Connection conn = new Database().getConnection();
			section = sectionService.getSection(conn, sectionName);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return section;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{sectionName}/delete")
	public void deleteSection(@PathParam("sectionName") String sectionName, Section section) {
		
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void postSection(Section section){
		try{
			Connection conn = new Database().getConnection();
			sectionService.postSection(conn, section);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/update")
	public void updateSection(Section section) {
		try {
			Connection conn = new Database().getConnection();
			sectionService.updateSection(conn, section);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
}
