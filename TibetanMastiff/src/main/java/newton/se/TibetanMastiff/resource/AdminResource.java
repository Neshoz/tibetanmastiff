package newton.se.TibetanMastiff.resource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import newton.se.TibetanMastiff.model.Admin;
import newton.se.TibetanMastiff.service.Database;
import newton.se.TibetanMastiff.service.admin.AdminService;
import newton.se.TibetanMastiff.service.admin.AdminServiceImpl;

@Path("/admins")
public class AdminResource {
	
	private AdminService adminService = new AdminServiceImpl();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Admin> getAllAdmins(){
		List<Admin> adminList = new LinkedList<Admin>();
		
		try{
			Connection conn = new Database().getConnection();
			adminList = adminService.getAllAdmins(conn);
		}
		catch(SQLException ex){
			ex.getMessage();
			ex.printStackTrace();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return adminList;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{username}")
	public Admin getAdmin(@PathParam("username") String username){
		Admin admin = null;
		
		try{
			Connection conn = new Database().getConnection();
			admin = adminService.getAdmin(conn, username);
		}
		catch(SQLException ex){
			ex.getMessage();
			ex.printStackTrace();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return admin;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void postAdmin(Admin admin){
		try{
			Connection conn = new Database().getConnection();
			adminService.postAdmin(conn, admin);
		}
		catch(SQLException ex){
			ex.getMessage();
			ex.printStackTrace();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/update")
	public void updateAdmin(Admin admin) {
		try {
			Connection conn = new Database().getConnection();
			adminService.updateAdmin(conn, admin);
		}
		catch(SQLException ex){
			ex.getMessage();
			ex.printStackTrace();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/delete")
	public void deleteAdmin(Admin admin) {
		try {
			Connection conn = new Database().getConnection();
			adminService.deleteAdmin(conn, admin);
		}
		catch(SQLException ex){
			ex.getMessage();
			ex.printStackTrace();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
}
