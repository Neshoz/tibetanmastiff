package newton.se.TibetanMastiff.resource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import newton.se.TibetanMastiff.model.Absent;
import newton.se.TibetanMastiff.service.Database;
import newton.se.TibetanMastiff.service.absent.AbsentService;
import newton.se.TibetanMastiff.service.absent.AbsentServiceImpl;

@Path("/absent")
public class AbsentResource {
	
	private AbsentService service = new AbsentServiceImpl();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{date}")
	public List<Absent> getAllAbsentChildren(@PathParam("date") String date) {
		List<Absent> absentChildren = null;
		
		try{
			Connection conn = new Database().getConnection();
			absentChildren = service.getAllAbsentChildren(conn, date);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return absentChildren;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{date}/{section}")
	public List<Absent> getAbsentChildrenInSection(@PathParam("date") String date, @PathParam("section") String section) {
		List<Absent> absentInSection = null;
		
		try{
			Connection conn = new Database().getConnection();
			absentInSection = service.getAbsentChildrenInSection(conn, date, section);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return absentInSection;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{date}/{section}/{ssn}")
	public Absent getAbsentChild(@PathParam("date") String date, @PathParam("section") String section, @PathParam("ssn") String socialSecurityNumber) {
		Absent absentChild = null;
		
		try{
			Connection conn = new Database().getConnection();
			absentChild = service.getAbsentChild(conn, socialSecurityNumber, date, section);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return absentChild;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public void postAbsentAttendance(Absent absent) {
		try{
			Connection conn = new Database().getConnection();
			service.postAbsentAttendance(conn, absent);
		}
		catch (SQLException ex){
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		}
		catch(Exception ex){
			ex.getMessage();
			ex.printStackTrace();
		}
	}
}
