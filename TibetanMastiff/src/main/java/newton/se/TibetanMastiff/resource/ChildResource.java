package newton.se.TibetanMastiff.resource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javafx.scene.Parent;
import newton.se.TibetanMastiff.model.Adult;
import newton.se.TibetanMastiff.model.Child;
import newton.se.TibetanMastiff.service.Database;
import newton.se.TibetanMastiff.service.child.ChildService;
import newton.se.TibetanMastiff.service.child.ChildServiceImpl;

@Path("/children")
public class ChildResource {

	private ChildService childService = new ChildServiceImpl();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Child> getAllChildren() {
		List<Child> children = new LinkedList<Child>();

		try {
			Connection conn = new Database().getConnection();
			children = childService.getAllChildren(conn);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}

		return children;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/sections/{sectionName}")
	public List<Child> getAllChildrenInSection(@PathParam("sectionName") String sectionName) {
		List<Child> children = new LinkedList<Child>();

		try {
			Connection conn = new Database().getConnection();
			children = childService.getAllChildrenInSection(conn, sectionName);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}

		return children;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{socialSecurityNumber}")
	public Child getChild(@PathParam("socialSecurityNumber") String socialSecurityNumber) {
		Child child = null;

		try {
			Connection conn = new Database().getConnection();
			child = childService.getChild(conn, socialSecurityNumber);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}

		return child;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{socialSecurityNumber}/pickupadults")
	public List<Adult> getAllowedPickUpAdults(@PathParam("socialSecurityNumber") String socialSecurityNumber) {
		List<Adult> adults = null;
		
		try {
			Connection conn = new Database().getConnection();
			adults = childService.getAllowedPickUpAdults(conn, socialSecurityNumber);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}
		
		return adults;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{sectionName}")
	public void postChild(@PathParam("sectionName") String sectionName, Child child) {
		try {
			Connection conn = new Database().getConnection();
			childService.postChild(conn, sectionName, child);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{socialSecurityNumber}/delete")
	public void deleteChild(@PathParam("socialSecurityNumber") String socialSecurityNumber, Child child) {
		try {
			Connection conn = new Database().getConnection();
			childService.deleteChild(conn, child);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/update")
	public void updateChild(Child child) {
		try {
			Connection conn = new Database().getConnection();
			childService.updateChild(conn, child);
		} catch (SQLException ex) {
			ex.printStackTrace();
			ex.getMessage();
			ex.getErrorCode();
		} catch (Exception ex) {
			ex.getMessage();
			ex.printStackTrace();
		}
	}

}
